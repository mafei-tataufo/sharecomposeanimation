package me.bakumon.sharecomposeanimation

import androidx.compose.animation.Animatable
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@ExperimentalAnimationApi
@Composable
fun AnimatedContentDemo() {
    Column(modifier = Modifier.padding(50.dp)) {
        var currentState by remember { mutableStateOf("A") }
        Button(onClick = {
            if (currentState == "A") {
                currentState = "B"
            } else {
                currentState = "A"
            }
        }) {
            Text("点击")
        }
        AnimatedContent(modifier = Modifier.padding(top = 10.dp), targetState = currentState) { state ->
            when (state) {
                "A" -> Text(text = "这是 A", fontSize = 20.sp)
                "B" -> Text(text = "这是 B", fontSize = 20.sp)
            }
            val ok = true
            val color = remember { Animatable(Color.Gray) }
            LaunchedEffect(ok) {
                color.animateTo(if (ok) Color.Green else Color.Red)
            }
            Box(Modifier.fillMaxSize().background(color.value))
        }
    }
}

@ExperimentalAnimationApi
@Preview
@Composable
fun AnimatedContentDemoPreview() {
    AnimatedContentDemo()
}