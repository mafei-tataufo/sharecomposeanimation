package me.bakumon.sharecomposeanimation

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.*
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

val CustomEasing = Easing { fraction ->
    fraction * fraction
}

@ExperimentalAnimationApi
@Composable
fun TweenDemo() {
    var targetValue by remember { mutableStateOf(0.dp) }

    Column(modifier = Modifier.padding(50.dp)) {

        Button(onClick = {
            targetValue = if (targetValue == 0.dp) 200.dp else 0.dp
        }) {
            Text(text = "动画")
        }

        val value by animateFloatAsState(
            targetValue = 1f,
            animationSpec = snap(delayMillis = 50)
        )

        val value1 by animateDpAsState(
            targetValue = targetValue,
            animationSpec = tween(
                durationMillis = 1000,
                delayMillis = 100,
                easing = FastOutSlowInEasing,
            )
        )
        TweenBox("FastOutSlowInEasing", value1)

        val value2 by animateDpAsState(
            targetValue = targetValue,
            animationSpec = tween(
                durationMillis = 1000,
                delayMillis = 100,
                easing = LinearOutSlowInEasing,
            )
        )
        TweenBox("LinearOutSlowInEasing", value2)

        val value3 by animateDpAsState(
            targetValue = targetValue,
            animationSpec = tween(
                durationMillis = 1000,
                delayMillis = 100,
                easing = FastOutLinearInEasing,
            )
        )
        TweenBox("FastOutLinearInEasing", value3)

        val value4 by animateDpAsState(
            targetValue = targetValue,
            animationSpec = tween(
                durationMillis = 1000,
                delayMillis = 100,
                easing = LinearEasing,
            )
        )
        TweenBox("LinearEasing", value4)

        val value5 by animateDpAsState(
            targetValue = targetValue,
            animationSpec = tween(
                durationMillis = 1000,
                delayMillis = 100,
                easing = CustomEasing,
            )
        )
        TweenBox("CustomEasing", value5)
    }
}

@Composable
private fun TweenBox(name: String, value: Dp) {
    Column(modifier = Modifier.padding(top = 20.dp)) {
        Text(text = name)
        Box(
            modifier = Modifier
                .padding(top = 5.dp)
                .width(value)
                .height(50.dp)
                .background(Color.Blue)
        ) {

        }
    }

}

@ExperimentalAnimationApi
@Preview
@Composable
fun TweenDemoPreview() {
    TweenDemo()
}