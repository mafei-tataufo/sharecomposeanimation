package me.bakumon.sharecomposeanimation

import androidx.compose.animation.Crossfade
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import me.bakumon.sharecomposeanimation.component.CatIcon

@ExperimentalAnimationApi
@Composable
fun CrossfadeDemo() {
    Column(modifier = Modifier.padding(start = 50.dp)) {
        var currentPage by remember { mutableStateOf("A") }
        Button(onClick = {
            if (currentPage == "A") {
                currentPage = "B"
            } else {
                currentPage = "A"
            }
        }, modifier = Modifier.padding(20.dp)) {
            Text("点击")
        }
        Crossfade(targetState = currentPage) { screen ->
            when (screen) {
                "A" -> CatIcon(resId = R.drawable.cat1)
                "B" -> CatIcon(resId = R.drawable.cat2)
            }
        }
    }
}

@ExperimentalAnimationApi
@Preview
@Composable
fun CrossfadeDemoPreview() {
    CrossfadeDemo()
}