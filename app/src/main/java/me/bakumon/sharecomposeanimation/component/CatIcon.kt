package me.bakumon.sharecomposeanimation.component

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import me.bakumon.sharecomposeanimation.R


@Composable
fun CatIcon(resId: Int = R.drawable.cat1) {
    Image(
        painter = painterResource(id = resId),
        contentDescription = "cat",
        modifier = Modifier
            .width(200.dp)
            .clip(RoundedCornerShape(20.dp))
    )
}