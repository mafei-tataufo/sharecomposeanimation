package me.bakumon.sharecomposeanimation

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import me.bakumon.sharecomposeanimation.component.CatIcon

@ExperimentalAnimationApi
@Composable
fun AnimatedVisibilityDemo() {
    var visible by remember { mutableStateOf(true) }

    Column(modifier = Modifier.padding(start = 50.dp)) {
        Button(onClick = { visible = !visible }, modifier = Modifier.padding(20.dp)) {
            Text(text = "点击")
        }
        AnimatedVisibility(visible = visible) {
            CatIcon()
        }
//        if (visible) {
//            CatIcon()
//        }
    }
}

@ExperimentalAnimationApi
@Preview
@Composable
fun AnimatedVisibilityDemoPreview() {
    AnimatedVisibilityDemo()
}