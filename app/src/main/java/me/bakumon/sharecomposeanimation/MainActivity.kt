package me.bakumon.sharecomposeanimation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.ui.Modifier
import me.bakumon.sharecomposeanimation.ui.theme.ShareComposeAnimationTheme

@ExperimentalAnimationApi
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ShareComposeAnimationTheme {
                // A surface container using the 'background' color from the theme
                Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colors.background) {
//                    AnimatedVisibilityDemo()
//                    AnimatedVisibilityStateDemo()
//                    AnimatedVisibilityEnterExitDemo()
//                    AnimatedVisibilityCustomDemo()
//                    AnimatedContentDemo()
//                    AnimatedContentNumberDemo()
//                    CrossfadeDemo()
//                    AnimatedContentSizeDemo()
//                    TweenDemo()
                    KeyframesDemo()
                }
            }
        }
    }
}
