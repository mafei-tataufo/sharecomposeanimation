package me.bakumon.sharecomposeanimation

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.MutableTransitionState
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import me.bakumon.sharecomposeanimation.component.CatIcon

@ExperimentalAnimationApi
@Composable
fun AnimatedVisibilityStateDemo() {

    Column(modifier = Modifier.padding(start = 50.dp)) {
        val state = remember {
            MutableTransitionState(false).apply {
                targetState = true
            }
        }
        Button(onClick = { state.targetState = !state.targetState }, modifier = Modifier.padding(20.dp)) {
            Text(text = "点击")
        }
        Text(
            text = when {
                state.isIdle && state.currentState -> "可见"
                !state.isIdle && state.currentState -> "正在消失"
                state.isIdle && !state.currentState -> "不可见"
                else -> "正在出现"
            }
        )
        AnimatedVisibility(visibleState = state) {
            CatIcon()
        }
    }
}

@ExperimentalAnimationApi
@Preview
@Composable
fun AnimatedVisibilityStateDemoPreview() {
    AnimatedVisibilityStateDemo()
}