package me.bakumon.sharecomposeanimation

import android.util.Log
import androidx.compose.animation.*
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@ExperimentalAnimationApi
@Composable
fun AnimatedVisibilityCustomDemo() {

    var visible by remember { mutableStateOf(true) }

    Column(modifier = Modifier.padding(start = 50.dp)) {
        Button(onClick = { visible = !visible }, modifier = Modifier.padding(20.dp)) {
            Text(text = "点击")
        }
        AnimatedVisibility(
            visible = visible,
            enter = fadeIn(),
            exit = fadeOut()
        ) {
            val background by transition.animateColor(label = "backgroundColor") { state ->
                if (state == EnterExitState.Visible)
                    Color.Blue
                else
                    Color.Red
            }
            Log.d("AnimatedVisibilityCustomDemo", "background=$background")
            Box(modifier = Modifier
                .size(128.dp)
                .background(background))
        }
    }
}

@ExperimentalAnimationApi
@Preview
@Composable
fun AnimatedVisibilityCustomDemoPreview() {
    AnimatedVisibilityCustomDemo()
}