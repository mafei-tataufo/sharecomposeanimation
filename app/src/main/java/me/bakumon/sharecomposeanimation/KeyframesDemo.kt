package me.bakumon.sharecomposeanimation

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.FastOutLinearInEasing
import androidx.compose.animation.core.LinearOutSlowInEasing
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.keyframes
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

@ExperimentalAnimationApi
@Composable
fun KeyframesDemo() {
    var targetValue by remember { mutableStateOf(0.dp) }

    Column(modifier = Modifier.padding(50.dp)) {

        Button(onClick = {
            targetValue = 200.dp
        }) {
            Text(text = "动画")
        }

        val value by animateDpAsState(
            targetValue = targetValue,
            animationSpec = keyframes {
                durationMillis = 2000
                0.dp at 0 with LinearOutSlowInEasing
                100.dp at 1000 with FastOutLinearInEasing
            }
        )

        TweenBox("keyframes", value)
    }
}

@Composable
private fun TweenBox(name: String, value: Dp) {
    Column(modifier = Modifier.padding(top = 20.dp)) {
        Text(text = name)
        Box(
            modifier = Modifier
                .padding(top = 5.dp)
                .width(value)
                .height(50.dp)
                .background(Color.Blue)
        ) {

        }
    }

}

@ExperimentalAnimationApi
@Preview
@Composable
fun KeyframesDemoPreview() {
    KeyframesDemo()
}