package me.bakumon.sharecomposeanimation

import androidx.compose.animation.*
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@ExperimentalAnimationApi
@Composable
fun AnimatedVisibilityEnterExitDemo() {

    var visible by remember { mutableStateOf(true) }

    Column {
        Button(onClick = { visible = !visible }, modifier = Modifier.padding(20.dp)) {
            Text(text = "点击")
        }
        AnimatedVisibility(
            visible = visible,
            enter = EnterTransition.None,
            exit = ExitTransition.None
        ) {
            Box(
                Modifier
                    .fillMaxSize()
                    .background(Color.DarkGray)
            ) {
                Box(
                    Modifier
                        .align(Alignment.Center)
                        .animateEnterExit(
                            enter = slideInVertically(),
                            exit = slideOutVertically()
                        )
                        .sizeIn(minWidth = 256.dp, minHeight = 64.dp)
                        .background(Color.Red)
                ) {

                }
            }
        }
    }
}

@ExperimentalAnimationApi
@Preview
@Composable
fun AnimatedVisibilityEnterExitDemoPreview() {
    AnimatedVisibilityEnterExitDemo()
}