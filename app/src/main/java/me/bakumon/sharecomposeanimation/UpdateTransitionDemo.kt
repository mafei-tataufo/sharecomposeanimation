package me.bakumon.sharecomposeanimation

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.animateColor
import androidx.compose.animation.core.animateDp
import androidx.compose.animation.core.spring
import androidx.compose.animation.core.tween
import androidx.compose.animation.core.updateTransition
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

enum class BoxState {
    Collapsed,
    Expanded
}

@ExperimentalAnimationApi
@Composable
fun UpdateTransitionDemo() {
    var currentState by remember { mutableStateOf(BoxState.Collapsed) }
    val transition = updateTransition(currentState, label = "transition")

    val size by transition.animateDp(label = "size", transitionSpec = {
        spring(
//            dampingRatio = Spring.DampingRatioHighBouncy
        )
    }) { state ->
        when (state) {
            BoxState.Collapsed -> 100.dp
            BoxState.Expanded -> 50.dp
        }
    }
    val color by transition.animateColor(label = "color",
        transitionSpec = {
            when {
                BoxState.Expanded isTransitioningTo BoxState.Collapsed ->
                    spring(stiffness = 50f)
                else ->
                    tween()
            }
        }
    ) { state ->
        when (state) {
            BoxState.Collapsed -> Color.Blue
            BoxState.Expanded -> Color.Red
        }
    }
    Box(
        modifier = Modifier
            .width(size)
            .height(size)
            .background(color)
    ) {

    }
}

@ExperimentalAnimationApi
@Preview
@Composable
fun UpdateTransitionDemoPreview() {
    UpdateTransitionDemo()
}