package me.bakumon.sharecomposeanimation

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@ExperimentalAnimationApi
@Composable
fun AnimatedContentSizeDemo() {

    Column(modifier = Modifier.padding(start = 50.dp, end = 50.dp)) {
        var message by remember { mutableStateOf("Hello") }
        Button(onClick = {
            message += " Hello"
        }, modifier = Modifier.padding(20.dp)) {
            Text(text = "点击")
        }
        Box(
            modifier = Modifier
                .background(Color.Blue)
                .animateContentSize()
        ) {
            Text(text = message, color = Color.White)
        }
    }
}

@ExperimentalAnimationApi
@Preview
@Composable
fun AnimatedContentSizDemoPreview() {
    AnimatedContentSizeDemo()
}