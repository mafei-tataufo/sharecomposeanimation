package me.bakumon.sharecomposeanimation

import androidx.compose.animation.*
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@ExperimentalAnimationApi
@Composable
fun AnimatedContentNumberDemo() {

    Column(modifier = Modifier.padding(50.dp)) {
        var count by remember { mutableStateOf(0) }
        Button(onClick = { count++ }) {
            Text("+1")
        }
        Spacer(modifier = Modifier.height(10.dp))
        Button(onClick = { count-- }) {
            Text("-1")
        }
        AnimatedContent(
            modifier = Modifier.padding(top = 30.dp),
            targetState = count,
            transitionSpec = {
                if (targetState > initialState) {
                    (slideInVertically({ height -> height }) + fadeIn()) with
                            (slideOutVertically({ height -> -height }) + fadeOut())
                } else {
                    (slideInVertically({ height -> -height }) + fadeIn()) with
                            (slideOutVertically({ height -> height }) + fadeOut())
                }.using(
                    SizeTransform(clip = false)
                )
            }
        ) { targetCount ->
            Text(text = "$targetCount", fontSize = 24.sp)
        }
    }
}

@ExperimentalAnimationApi
@Preview
@Composable
fun AnimatedContentNumberDemoPreview() {
    AnimatedContentNumberDemo()
}